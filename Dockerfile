FROM ruby:2.6.6-buster

WORKDIR /app

COPY . /app
RUN chmod +x /app/main.rb

# install dependencies needed
RUN bundle install

VOLUME ["/gitlab-ci-processor"]
WORKDIR /gitlab-ci-processor

ENTRYPOINT ["ruby", "/app/main.rb"]
